package com.availity.spark.provider

import org.apache.log4j.Logger
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.sql.types.{DateType, IntegerType, StringType, StructType}
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions.{array, avg, col, collect_list, concat, count, lit, month}

object ProviderRoster  {

  def main(args: Array[String]): Unit = {
    println("start of the process")
    process()
  }
  def process(): Unit ={
    val log = Logger.getLogger(getClass.getName)

    val spark: SparkSession = SparkSession.builder()
      .appName("ProviderReport").master("local")
      .getOrCreate()

    //    val provider_path = getClass.getResource("/providers.csv").getPath
    val project_data_path = "file:/C:/Users/swarg/git/provider-roster/data"
    val provider_path = project_data_path + "/providers.csv"
    val visits_path = project_data_path + "/visits.csv"

    log.info(f"path:$provider_path")

    val visit_schema = StructType(
      Array(
        StructField("visit_id", IntegerType),
        StructField("provider_id", IntegerType),
        StructField("visit_date", DateType)
      )
    )

    try{

      val provider_df = spark.read.options(Map("delimiter"->"|", "header"->"true")).format("csv").load(provider_path)
      val visits_df = spark.read.format("csv").option("header", "false").schema(visit_schema).format("csv").load(visits_path)
      provider_df.show(20,0)
      provider_df.printSchema()
      visits_df.show(10,0)
      visits_df.printSchema()

      val visit_count_df = visits_df.groupBy("provider_id").agg(count("*").alias("provider_visits"))
      val provider_visit_df = provider_df.join(visit_count_df, Seq("provider_id")).select(col("provider_id"), concat(col("first_name"), lit(" "), col("middle_name"), lit(" "), col("last_name")).as("name"),
        col("provider_specialty"), col("provider_visits"))

      provider_visit_df.show(10,0)
      log.info(provider_visit_df.count())
      provider_visit_df.printSchema()
      provider_visit_df.write.mode(SaveMode.Overwrite).partitionBy("provider_specialty").json(project_data_path + "/output1")
      val provider_visit_month_df = visits_df.groupBy(col("provider_id"),month(col("visit_date")).as("visit_month")).agg(count("*").alias("month_visits"))

      provider_visit_month_df.show(10,0)
      provider_visit_month_df.printSchema()

      provider_visit_month_df.write.mode(SaveMode.Overwrite).json(project_data_path + "/output2")

    } catch {
      case e: Exception => println("Exception occurred : " + e)
    }


  }
}
